CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The ajax link module give tools for themers and developers to easily implements ajax links.

Core implements AjaxResponse methods, but if you already have a page with content you want to append or replace in an area, you have to define custom AjaxResponse to each cases.

This module implements a generic AjaxResponse which load and render page accessible by the link you click on, and only respond with the markup of the array to replace.

Nothing to configure from the UI, everything happens by data attributes on the links, so it's easy to access for themers.

The `ajax_link/ajaxLink` library needs to be attached on pages where you want to use ajax link.

Example :

I want to load the next page of the product list in the area matching the class products-list.
Define the link as an ajax link adding 'ajax-link' class to the link markup.
Add the 'data-ajax-link-selector' data attribute with css selector where to find and replace content : '.products-list'.
<code>
  <a href="{{ url }}" class="ajax-link" data-ajax-link-selector=".products-list">This is an ajax link</a>
</code>

And load library in preprocess :
<code>
$variables['#attached']['library'][] = 'ajax_link/ajaxLink';
</code>

or twig :
<code>
{{ attach_library('ajax_link/ajaxLink') }}
</code>

Here is available settings for the link :

* Class :
  * ajax-link : **required**, Let the module know this link is an ajax link.
  * ajax-link-auto : optional, when the link will be 'visible' in the browser, the link will be automatically clicked. With this option, you can have an infinite scroll pager for example.
  * ajax-link-hidden : optional, hide the link. Compatible with ajax-link-auto.
* Data attributes :
  * data-ajax-link-selector: **required**, define the area to get content and to replace/append.
  * data-ajax-link-method: optional, Define the method to use : replace (default) OR append.
  * data-ajax-link-history: optional, Set to 1 if you want the browser history to reflect the clicked link. Default to 0.
  * data-ajax-link-remove-after-execution: optional, remove link after execution. Default to true. Set value to false to keep
the link displayed.

INSTALLATION
------------

 * Install this module normally, just like you would install a
   contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
   further information.

CONFIGURATION
-------------

    1. Nothing to do, everything appends in twig or markup. 

MAINTAINERS
-----------

 * Fabien Clément (Goz) - https://www.drupal.org/u/goz

Supporting organizations:

 * IOSAN  - https://www.drupal.org/iosan
